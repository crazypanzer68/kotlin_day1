import kotlin.math.max

fun main(args: Array<String>) {
    var arrays = arrayOf(5, 55, 200, 1, 3, 5, 7)
    var max = findMaxValue(arrays)
    println("Max value is $max")
}

fun findMaxValue(values: Array<Int>): Int {
    var max = values[0]
    for (i in values.indices) {
        if (values[i] > max) {
            max = values[i]
        }
    }
    return max
}
