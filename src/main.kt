abstract class Person(var name: String, var surname: String, var gpa: Double) {
    constructor(name: String, surname: String) : this(name, surname, 0.0) {

    }

    abstract fun goodBoy(): Boolean
    open fun getDetails(): String {
        return "$name $surname has score $gpa"
    }
}

class Student(name: String, surname: String, gpa: Double, var department: String) : Person(name, surname, gpa) {
    constructor(name: String, surname: String) : this(name, surname, 0.0, "CMU") {

    }

    override fun goodBoy(): Boolean {
        return gpa > 2.0
    }

    override fun getDetails(): String {
        return super.getDetails() + " and study in $department"
    }
}

data class Teacher(var name: String, var courseName: String, var ShirtColor: Color)

enum class Color {
    RED, GREEN, BLUE
}

fun main(args: Array<String>) {
    val no1: Student = Student("Somchai", "Pitak", 2.50, "CAMT")
    val no2: Student = Student("Prayuth", "SonOf")
    val no3: Student = Student(surname = "Prayuth", name = "SonOf")
    println(no1.getDetails() + " good boy?->" + no1.goodBoy())
    println(no2.getDetails() + " good boy?->" + no2.goodBoy())
    println(no3.getDetails())
    var teacher1: Teacher = Teacher("Somsak", "Democratic", Color.GREEN)
    var teacher2: Teacher = Teacher("Somsak", "Democratic", Color.BLUE)
    var teacher3: Teacher = Teacher("Somnamna", "Mathmatics", Color.RED)
    val (teacherName, teacherCourse, teacherShirtColor) = teacher3
    println(teacher1)
    println(teacher1.equals(teacher2))
    println(teacher2.courseName)
    println(teacher2.name)
    println(teacher2.ShirtColor)
    println("$teacherName teaches $teacherCourse in $teacherShirtColor shirt")
    var teacher4 = teacher1.copy()
    println(teacher4)
    println(teacher1.equals(teacher4))
    var adHoc = object {
        var x: Int = 1
        var y: Int = 2
    }
    println(adHoc)
    println(adHoc.x + adHoc.y)
}
