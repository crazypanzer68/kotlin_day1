fun main(args: Array<String>) {
    val x: Int = 1
    val y: Int = 2
    val z = x + y
    println("Hello, World! $z")
    var data: Int
    var dataNulable: Int?
    data = 2
    dataNulable = null
    println(y == data)
    println(y === data)
    helloWorld()
    helloMore("Prayuth!")
    helloMore("Sanchai!")
    yourName("Pisit", "Ruangpornvisuti")
    gradeReport("Somchai", 3.33)
    gradeReport()
    gradeReport(name = "Nobita")
    gradeReport(gpa = 2.50)
    println(isOdd(3))
    println(isOdd(6))
    println(getAbbreviation('A'))
    println(getAbbreviation('B'))
    println(getAbbreviation('C'))
    println(getAbbreviation('D'))
    println(getAbbreviation('F'))
    println(getGrade(50))
    println(getGrade(70))
    println(getGrade(80))
    println(getGrade(100))
    println(getGrade(101))
    println(subGrade(101))
    println(subGrade(100))
}

fun helloWorld(): Unit {
    println("Hello World!")
}

fun helloMore(text: String): Unit {
    println("Hello $text")
}

fun yourName(fname: String = "Pisti", lname: String = "Ruangpornvistui"): Unit {
    gradeReport("$lname $fname", 3.33)
}

fun gradeReport(
    name: String = "annonymous",
    gpa: Double = 0.00
): Unit = println("mister $name gpa:is $gpa")

fun isOdd(value: Int): String {
    if (value.rem(2) == 0) {
        return "$value is even Value"
    } else {
        return "$value is odd Value"
    }
}

fun getAbbreviation(abbr: Char): String {
    when (abbr) {
        'A' -> return "Abnormal"
        'B' -> return "Bad Boy"
        'F' -> return "Fantastic"
        'C' -> {
            println("Not Smart")
            return "Cheap"
        }
        else -> return "Hello"
    }
}

fun getGrade(score: Int): String? {
    var grade: String?
    when (score) {
        in 0..50 -> grade = "F"
        in 51..70 -> grade = "C"
        in 71..80 -> grade = "B"
        in 81..100 -> grade = "A"
        else -> grade = null
    }
    return grade
}

fun subGrade(score: Int): String? {
    var grade: String? = getGrade(score)
    if (grade == null) {
        return "Null grade ???"
    } else {
        return "Your grade is $grade " + getAbbreviation(grade.toCharArray()[0])
    }
}