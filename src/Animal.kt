abstract class Animal(var type: String, var leg: Int, var food: String) {
    constructor(type: String, leg: Int) : this(type, leg, "meat") {

    }

    abstract fun getSound(): String
    open fun getAnimal(): String {
        return "$type has $leg leg like to eat $food"
    }
}

class Dog(type: String, leg: Int, food: String, var name: String) : Animal(type, leg, food) {
    override fun getSound(): String {
        return "sound like BARK!"
    }

    override fun getAnimal(): String {
        return super.getAnimal() + " name $name"
    }
}

class Lion(type: String, leg: Int, food: String) : Animal(type, leg, food) {
    constructor(type: String, leg: Int) : this(type, leg, "Meat") {

    }

    override fun getSound(): String {
        return "sound like ROAR!"
}

    override fun getAnimal(): String {
        return super.getAnimal()
    }
}

fun main(args: Array<String>) {
    val dog: Dog = Dog("Dog", 4, "Dog Food", "Jame")
    val lion: Lion = Lion("Lion", 4)
    val lion2:Lion = Lion("Lion",5,"Vegan")
    println(dog.getAnimal()+" "+dog.getSound())
    println(lion.getAnimal()+" "+lion.getSound())
    println(lion2.getAnimal()+" "+lion.getSound())
}
